import {my_alpha_number_t, sum, my_size_alpha_t, my_display_alpha_t,
  my_array_alpha_t, my_is_posi_neg_t, fibo, my_display_alpha_reverse_t, my_length_array_t} from '../src';

describe('Index', () => {
  it('should be return 12 in string when I say 12', () => {
    expect(my_alpha_number_t(12)).toBe('12');
  });
  it('should be return toto when a say toto in string', () => {
    expect(my_alpha_number_t(12)).toBe('12');
  });
  it('should be return undefined in string when I say nothing', () => {
    expect(my_alpha_number_t()).toBe('undefined');
  });

  it('should be had 7 when I sum 3 and 4 ', () => {
    expect(sum(3,4)).toBe(7);
  });
  it('should be had 0 when I sum 3 and ""', () => {
    expect(sum(3,"")).toBe(0);
  });
  it('should be had 0 when I sum nothing', () => {
    expect(sum()).toBe(0);
  });

  it('should be had the size 4 for the string toto', () => {
    expect(my_size_alpha_t('toto')).toBe(4);
  });
  it('should be had the size 0 when is not a string', () => {
    expect(my_size_alpha_t(20)).toBe(0);
  });
  it('should be had the size 0 when is empty', () => {
    expect(my_size_alpha_t()).toBe(0);
  });

  it('should be had string abcdefghijklmnopqrstuvwxyz when I say 20', () => {
    expect(my_display_alpha_t(20)).toBe('abcdefghijklmnopqrstuvwxyz');
  });
  it('should be had string abcdefghijklmnopqrstuvwxyz when is empty ', () => {
    expect(my_display_alpha_t()).toBe('abcdefghijklmnopqrstuvwxyz');
  });

  it('should be had empty table if I say nothing', () => {
    expect(my_array_alpha_t()).toEqual([]);
  });
  it('should be had string table [t,o,t,o] when I say toto', () => {
    expect(my_array_alpha_t('toto')).toEqual(['t','o','t','o']);
  });
  it('should be had empty table if I say [t,o,t,o]', () => {
    expect(my_array_alpha_t(['t','o','t','o'])).toEqual([]);
  });
  it('should be had empty table if I say [2,4,5]', () => {
    expect(my_array_alpha_t([2,4,5])).toEqual([]);
  });
  it('should be had empty table if I say 24', () => {
    expect(my_array_alpha_t(24)).toEqual([]);
  });

  it('should be return POSITIF if I say 20 ', () => {
    expect(my_is_posi_neg_t(20)).toBe('POSITIF');
  });
  it('should be return NEGATIVE if I say -20', () => {
    expect(my_is_posi_neg_t(-20)).toBe('NEGATIVE');
  });
  it('should be return POSITIF if I say toto', () => {
    expect(my_is_posi_neg_t('toto')).toBe('POSITIF');
  });
  it('should be return POSITIF if I say nothing', () => {
    expect(my_is_posi_neg_t()).toBe('POSITIF');
  });

  it('should be return 0 if I say 0', () => {
    expect(fibo(0)).toBe(0);
  });
  it('should be return 1 if I say 2', () => {
    expect(fibo(2)).toBe(1);
  });
  it('should be return 8 if I say 6', () => {
    expect(fibo(6)).toBe(8);
  });
  it('should be return 12586269025 if I say 50', () => {
    expect(fibo(50)).toBe(12586269025);
  });

  it('should be return ', () => {
    const result = 'abcdefghijklmnopqrstuvwxyz'.split().reverse();
    expect(my_display_alpha_reverse_t())===(result);
  });

  it('should be return 0 when the table is empty', () => {
    expect(my_length_array_t([])).toBe(0);
  });
  it('should be return 0 when the table is [t]', () => {
    expect(my_length_array_t(['t'])).toBe(1);
  });
  it('should be return 0 when I say 6', () => {
    expect(my_length_array_t(6)).toBe(0);
  });


});
